#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 13 11:03:32 2018

@author: mxm7832
"""

from cc_segmentation_train import Training
import os
os.environ["CUDA_VISIBLE_DEVICES"]="1"

def main(Params,targetPath,contextPath,modelName):
    #TRAINING
    model,score=fine_tune_2branch(Params,targetPath,contextPath)
    model.save('../../../output/CNN_classifiers/'+Params['exp_name']+'.h5')

    #CROSS VALIDATION
    #recognition_rates=cross_validation(Params,modelName,targetPath,contextPath)    
    #rates=multiple_run(Params,modelName,targetPath,contextPath)
                              
        
def fine_tune_vgg16(params,targetPath):
    
    testImgPath=targetPath+'test/'
    trainImgPath= targetPath+'train/'
    
    train=Training(**params)  
    model=train.fineTune_VGG16_model(trainImgPath,testImgPath)
    scores=train.evaluate_model(model,trainImgPath)
    
    return model,scores

def fine_tune_2branch(params,targetPath,contextPath):  
    
    train=Training(**params)
    #define paths
    trainImgPath1= targetPath+'train/'
    trainImgPath2= contextPath +'train/'
    testImgPath1 = targetPath+'test/'
    testImgPath2 = contextPath +'test/'
    
    model=train.two_branch_model(trainImgPath1,trainImgPath2,testImgPath1,testImgPath2)
    scores=train.evaluate_model(model,testImgPath1,testImgPath2)

    #MM: giving training path to evaluation for validation sets!!!
    #scores=train.evaluate_model(model,trainImgPath1,trainImgPath2)
    return model,scores

def fine_tune_3branch(params,ccPath,contextPath):  
    
    train=Training(**params)
    #define paths
    trainImgPath1= ccPath+'train/'
    trainImgPath2= contextPath +'train/'
    testImgPath1 = ccPath+'test/'
    testImgPath2 = contextPath +'test/'
     
    model=train.three_branch_model(trainImgPath1,trainImgPath2,testImgPath1,testImgPath2)
    train.evaluate_model(model,testImgPath1,testImgPath2)

def make_kfolds_data(k,trainList,outputdir):
    f=open(trainList)
    lines= f.readlines()      
    fold_Size=int(len(lines)/k)
        
    for i in range(k):
        valid_lines=lines[i*fold_Size:(i+1)*fold_Size]
        #print('valid labels',len(valid_lines))
        train_lines=[i for i in lines if i not in valid_lines]
        #print('trainlabels',len(train_lines))
        path=outputdir+'fold'+str(i)+'/'
        os.mkdir(path)
        
        train=open(path+'train.txt','w')
        for line in train_lines:
            train.write(line)
        train.close()
        
        valid=open(path+'valid.txt','w')
        for line in valid_lines:
            valid.write(line)
        valid.close()                     
        
def cross_validation(params,modelName,targetImgPath,contextImgPath,k=5):
    #update train and valid label path 
    recognition_rates=[]
    for i in range(k):
        print('**** Training fold'+str(i)+' *******')
        params['trainLabelPath']='../Data/typeset/infty_segmentation/labels/fold'+str(i)+'/train.txt'
        params['validLabelPath']='../Data/typeset/infty_segmentation/labels/fold'+str(i)+'/valid.txt'
         #set validation set as test set to recird the accuracy on validation
        params['testLabelPath']='../Data/typeset/infty_segmentation/labels/fold'+str(i)+'/valid.txt'       
        params['exp_name']=modelName+'fold'+str(i)
        model,score=fine_tune_vgg16(params,targetImgPath)
        #model,score=fine_tune_2branch(params,targetImgPath,contextImgPath)
        model.save('../../../output/CNN_classifiers/seg_finetune+tanh+geo/model_fold'+str(i)+'.h5')
        recognition_rates.append(score)
        
    print(recognition_rates)   
    return recognition_rates

def multiple_run(params,modelName,targetImgPath,contextImgPath,k=4):
    #update train and valid label path 
    recognition_rates=[]
    for i in range(k):
        print('**** Training model'+str(i)+' *******')     
        params['exp_name']='run'+str(i)+'_'+modelName
        model,score=fine_tune_vgg16(params,targetImgPath)
        print("SCORE:",score)
        #fine_tune_2branch(params,targetImgPath,contextImgPath)
        model.save('../../../output/CNN_classifiers/'+params['exp_name']+'.h5')
        recognition_rates.append(score)
        
    print(recognition_rates)   
    return recognition_rates

if __name__ == '__main__':
    
    Params = {'dim': (224,224,3),
      'b_size': 32, 
      'n_classes': 2,
      'learning_rate': 0.00001,
      'datasetName': 'infty',
      'n_epochs': 70, 
      'testLabelPath':'cnn_data/segTarget/labels/test.txt',
      'trainLabelPath':'cnn_data/segTarget/labels/train.txt',
      'validLabelPath':'cnn_data/segTarget/labels/valid.txt',
      'exp_name': 'Segmentation_lateFusion_R1.75_us+context',
      'featurePath': 'cnn_data/spatialFeat/segment/', #geofeat
      }  
    #Make sure you generated target and context images for this task using 'infty_img_generation.py'
    targetPath='cnn_data/segTarget/'
    contextPath='cnn_data/segContext/'
    modelName='mm_test_release'

    main(Params,targetPath,contextPath,modelName)
