#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 18:55:50 2018

@author: mxm7832
"""
import keras
import numpy as np
#from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input  
from PIL import Image,ImageFilter
import cv2
import os

def relation_class_index():
    relation={}
    class_list=['RSUP', 'HORIZONTAL', 'NoRelation', 'RSUB', 'UNDER', 'LSUB', 'UPPER', 'LSUP','PUNC']
    for i in range(9):
        relation[str(i)]= class_list[i]
    return relation
        

class data_gen(keras.utils.Sequence):
    
    def __init__(self, img_path, labels, batch_size,dataset_name,feat=None):
        self.img_path=img_path
        self.file_names=[v for v in labels.keys()]
        self.labels= [v[1] for v in labels.values()]
        self.batch_size = batch_size
        self.dataset_name=dataset_name
        self.feat=feat

    def __len__(self):
        return int(np.ceil(len(self.file_names) / float(self.batch_size)))

    def __getitem__(self, idx):
        batch_filename = self.file_names[idx * self.batch_size:(idx + 1) * self.batch_size]        
        batch_label = self.labels[idx * self.batch_size:(idx + 1) * self.batch_size]
       # print(batch_filename,batch_label)
        batch_images=[] 
        features=[]   
        for imgname in batch_filename:
            if self.dataset_name=='infty':            
                im =Image.open(self.img_path+imgname+'.PNG')
                #im = im.filter(ImageFilter.BLUR)
            elif self.dataset_name=='crohme':   
                #im=Image.open(self.img_path+imgname.split('_')[0]+'/'+imgname.split('_')[1]+'.png')
                im=cv2.imread(self.img_path+imgname.split('_')[0]+'/'+imgname.split('_')[1]+'.png') 
                im=Image.fromarray(im, 'RGB')
                im = im.filter(ImageFilter.FIND_EDGES)

            im=(np.array(im)).astype('float32') / 255.0
            im = np.expand_dims(im, axis=0)
            im = preprocess_input(im)
            batch_images.extend(im)
            if self.feat:
                feat=self.feat[imgname]
                #replace negative values with zero(Relu)
                #feat[feat<0]=0
                features.append(feat)
                
        if features:
            return [np.array(batch_images),np.array(features)],np.array(batch_label)
        else:
            return np.array(batch_images),np.array(batch_label)

                
class data_gen_2D(keras.utils.Sequence):
    
    def __init__(self, img_path1,img_path2,labels, batch_size,dataset_name,feat=None):
        self.img_path1=img_path1
        self.img_path2=img_path2
        self.file_names=[v for v in labels.keys()]
        self.labels= [v[1] for v in labels.values()]
        self.batch_size = batch_size
        self.dataset_name=dataset_name
        self.feat=feat
    def __len__(self):
        return int(np.ceil(len(self.file_names) / float(self.batch_size)))
    
    def preProcess_img(self,im):
        #im = im.filter(ImageFilter.BLUR)
        im=(np.array(im)).astype('float32') / 255.0
        im = np.expand_dims(im, axis=0)
        img = preprocess_input(im)
        return img

    def __getitem__(self, idx):
        batch_filename = self.file_names[idx * self.batch_size:(idx + 1) * self.batch_size]        
        batch_label = self.labels[idx * self.batch_size:(idx + 1) * self.batch_size]
        #print(batch_filename)
        target_batch_images=[] 
        context_batch_images=[]
        features=[]
        for imgname in batch_filename:
            #if self.dataset_name=='infty':            
            im = Image.open(self.img_path1+imgname+'.PNG')
            im = im.filter(ImageFilter.BLUR)
            target_im=self.preProcess_img(im)
            #check if context exist for symbol:
            if not os.path.isfile(self.img_path2+imgname+'.PNG'):
                #print('context not found')
                im = Image.open(self.img_path1+imgname+'.PNG')
                #resize the traget to context size and pass to context channel
                #im = im.resize(target_im.shape[:2], Image.ANTIALIAS)
            else:
                im= Image.open(self.img_path2+imgname+'.PNG')
                
            context_im=self.preProcess_img(im)

            target_batch_images.extend(target_im)
            context_batch_images.extend(context_im)
            if self.feat:
                feat=self.feat[imgname]
                #replace negative values with zero(Relu)
                #feat[feat<0]=0
                features.append(feat)

        if features:
            return [np.array(target_batch_images),np.array(context_batch_images),np.array(features)],np.array(batch_label)
        else:    
            return[np.array(target_batch_images),np.array(context_batch_images)],np.array(batch_label)

#feeding parent,child,context(us + 0.75 around) to 3 channel model   
class seg_gen_3D(keras.utils.Sequence):
    
    def __init__(self, CC_img_path,context_img_path,labels, batch_size,dataset_name,feat=None):
        self.context_img_path=context_img_path
        self.CC_img_path=CC_img_path
        self.file_names=[v for v in labels.keys()]
        self.labels= [v[1] for v in labels.values()]
        self.batch_size = batch_size
        self.dataset_name=dataset_name
        self.feat=feat
        
    def __len__(self):
        return int(np.ceil(len(self.file_names) / float(self.batch_size)))
    
    def preProcess_img(self,im):
        im = im.filter(ImageFilter.BLUR)
        im=(np.array(im)).astype('float32') / 255.0
        im = np.expand_dims(im, axis=0)
        img = preprocess_input(im)
        return img
    
    def __getitem__(self, idx):
        batch_filename = self.file_names[idx * self.batch_size:(idx + 1) * self.batch_size]        
        batch_label = self.labels[idx * self.batch_size:(idx + 1) * self.batch_size]
       # print(batch_filename,batch_label)
        parent_batch_images=[]
        child_batch_images=[] 
        context_batch_images=[]
        features=[]
        for imgname in batch_filename:
            parts=imgname.split('_')
            p_imgname=parts[0]+'_'+parts[1]
            c_imgname=parts[0]+'_'+parts[2]
            #if self.dataset_name=='infty':                        
            im = Image.open(self.CC_img_path+p_imgname+'.PNG')
            p_im=self.preProcess_img(im)
            parent_batch_images.extend(p_im)
            
            im = Image.open(self.CC_img_path+c_imgname+'.PNG')
            c_im=self.preProcess_img(im)
            child_batch_images.extend(c_im)
            
            im = Image.open(self.context_img_path+imgname+'.PNG')
            context_im=self.preProcess_img(im)
            context_batch_images.extend(context_im)
            
            if self.feat:
                feat=self.feat[imgname]
                #replace negative values with zero(Relu)
                #feat[feat<0]=0
                features.append(feat)

        if features:
            return [np.array(parent_batch_images),np.array(child_batch_images),
                    np.array(context_batch_images),np.array(features)],np.array(batch_label)
        else:    
            return [np.array(parent_batch_images),np.array(child_batch_images),
                    np.array(context_batch_images)],np.array(batch_label)