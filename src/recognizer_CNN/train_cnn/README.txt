============================================
CNN models
============================================
Training models for LPGA_CNN:

 - cc_segmentation_train.py: Segmentor and parser
 - symbol_recognition_train.py: symbol Classifier

============================================
Train new classifiers
============================================
To train and evaluate each classifiers performance individually:

 - run_cc_segmentation.py
 - run_symbol_recognition.py
 - run_symbol_parsing.py

================================================
Others
================================================
 - infty_img_generation.py: to generate target and context images from original 
infty dataset for training CNNs on pairs of CCs, symbols pairs, or symbols

  ** Usage: python3 infty_img_generation.py [Task] [dataset]
  ** Example: python3 infty_img_generation.py parTarget test

[Task]:['symContext', 'symTarget', 'segContext', 'segTarget', 'parContext', 'parTarget']
[dataset]: ['test','train']

 - Data_generator.py: feeding data to CNN models 
 - load_INFTY.py: load dataset and labels 
