#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 29 16:22:59 2018

@author: mxm7832
"""
from symbol_recognition_train import Training
import os
os.environ["CUDA_VISIBLE_DEVICES"]="1"

def main (params,modelName,targetPath,contextPath):
    
    model,score=fine_tune_2branch(params,targetPath,contextPath)
    model.save('../../../output/CNN_classifiers/'+params['exp_name']+'.h5')

    #multiple_run(Params,modelName,targetPath,contextPath)~

def fine_tune_vgg16(params,trainImgPath,testImgPath):
    
    train=Training(**params)    
    model=train.fineTune_VGG16_model(trainImgPath,testImgPath)
    pred,acc=train.evaluate_model(model,testImgPath)

def fine_tune_2branch(params,targetPath,contextPath):  
    
    train=Training(**params)
    #define paths
    trainImgPath1= targetPath+'train/'
    trainImgPath2= contextPath +'train/'
    testImgPath1 = targetPath+'test/'
    testImgPath2 = contextPath +'test/' 
    
    model=train.two_branch_model(trainImgPath1,trainImgPath2,testImgPath1,testImgPath2)
    #MM: give the test path for test set
    score=train.evaluate_model(model,trainImgPath1,trainImgPath2)
    return model,score

def cross_validation(params,modelName,targetImgPath,contextImgPath,k=5):
    #update train and valid label path 
    recognition_rates=[]
    for i in range(k):
        print('**** Training fold'+str(i)+' *******')
        params['trainLabelPath']='../Data/typeset/infty_classification/labels/fold'+str(i)+'/train.txt'
        params['validLabelPath']='../Data/typeset/infty_classification/labels/fold'+str(i)+'/valid.txt'
         #set validation set as test set to recird the accuracy on validation
        params['testLabelPath']='../Data/typeset/infty_classification/labels/fold'+str(i)+'/valid.txt'       
        params['exp_name']=modelName+'fold'+str(i)
        model,score=fine_tune_2branch(params,targetImgPath,contextImgPath)
        #model,score=fine_tune_2branch(params,targetImgPath,contextImgPath)
        model.save('../../../output/CNN_classifiers/rec_lateFusion_R4/model_fold'+str(i)+'.h5')
        recognition_rates.append(score)
        
    print(recognition_rates)   
    return recognition_rates 

def multiple_run(params,modelName,targetImgPath,contextImgPath,k=4):
    #update train and valid label path 
    recognition_rates=[]
    for i in range(k):
        print('**** Training model'+str(i)+' *******')     
        params['exp_name']='run'+str(i)+'_'+modelName
        model,score=fine_tune_2branch(params,targetImgPath,contextImgPath)
        model.save('../../../output/CNN_classifiers/rec_lateFusion_R4/model_fold'+str(i)+'.h5')
        
    print(recognition_rates)   
    return recognition_rates
   
if __name__ == '__main__':
    Params = {'dim': (224,224,3),
          'b_size': 32, 
          'n_classes': 207, #213 before merge
          'learning_rate': 0.0001,
          'datasetName': 'infty',
          'n_epochs': 40, #for cross validation purpose- before it was 30
          'merge_labels':True,
          'testLabelPath':'cnn_data/symTarget/labels/test.txt',
          'trainLabelPath':'cnn_data/symTarget/labels/train.txt',
          'validLabelPath':'cnn_data/symTarget/labels/valid.txt',
          'exp_name': 'Classification_release_test',
          }  
    #Make sure you generated target and context images for this task using 'infty_img_generation.py'
    modelName='rec_lateFusion_R4'
    targetPath='cnn_data/symTarget/'
    contextPath='cnn_data/symContext/'

    main (Params,modelName,targetPath,contextPath)
