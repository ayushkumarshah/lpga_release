#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 18:52:39 2018

@author: mxm7832
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io, measure
#from symbol_classification_vgg16_ML import encodeLabels, encode_testLabels, load_label
from Data_generator import data_gen,data_gen_2D
import multiprocessing
from keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping 
from keras.applications.vgg16 import VGG16
from keras.layers import Dense, GlobalAveragePooling2D,Flatten,Dropout,concatenate
from keras.models import Model
from keras.optimizers import SGD,RMSprop,Nadam
#from load_CROHME import CROHME_labels
from load_INFTY import infty_labels
from sklearn.metrics import confusion_matrix
#from symbol_classification_vgg16_RF import report_performance
from keras.utils import plot_model       


class Training():
    #TODO: make config files for different experiments to get parameters from
    def __init__(self, dim,b_size,n_classes,learning_rate,datasetName,n_epochs,merge_labels,
                 trainLabelPath,testLabelPath,validLabelPath,exp_name):
        #define network parameters
        self.dim=dim
        self.b_size=b_size
        self.n_classes=n_classes
        self.learning_rate=learning_rate
        self.dataset= datasetName
        self.n_epochs=n_epochs
        self.merge_labels=merge_labels
        self.experimentName=exp_name
        #define labels
        if self.dataset=='infty':
            train_labels,test_labels,valid_labels,funcs=infty_labels(trainLabelPath,testLabelPath,validLabelPath,merge=self.merge_labels)
        elif self.dataset=='crohme':
            train_labels,test_labels,valid_labels,funcs=CROHME_labels(trainLabelPath,testLabelPath,validLabelPath)
        
        self.funcs=funcs
        self.transformers=funcs
        self.train_labels=train_labels
        self.test_labels=test_labels
        self.valid_labels=valid_labels
        '''
        self.trainImgPath=trainImgPath
        self.testImgPath=testImgPath
        '''   
    def two_branch_model(self,trainImgPath1,trainImgPath2,testImgPath1,testImgPath2):
        
        my_training_generator=data_gen_2D(trainImgPath1,trainImgPath2,self.train_labels,batch_size=self.b_size,
                                          dataset_name=self.dataset)
        #***MM: chnage paths to test later****
        my_validation_generator=data_gen_2D(trainImgPath1,trainImgPath2,self.valid_labels,batch_size=64,
                                            dataset_name=self.dataset) 
        
        base_model1 = VGG16(weights='imagenet', include_top=False,input_shape =(224,224,3))
        x1 = base_model1.output
        x1 = GlobalAveragePooling2D()(x1) 
        #x1 = Dense(512, activation='relu')(x1)
        for layer in base_model1.layers:
            layer.trainable = False
            
        base_model2 = VGG16(weights='imagenet', include_top=False,input_shape = (224,224,3)) 
        x2 = base_model2.output
        x2 = GlobalAveragePooling2D()(x2) 
        #x2 = Dense(512, activation='relu')(x2)         
        for layer in base_model2.layers:
            layer.name = layer.name + str("_2")
            layer.trainable = False
            
        x=concatenate([x1,x2])
        x = Dense(1024, activation='relu')(x)
        x= Dropout(0.5)(x)
        predictions = Dense(self.n_classes, activation='softmax')(x) 
        #plot_model(model,to_file='demo.png',show_shapes=True)         
        model = Model(inputs=[base_model1.input,base_model2.input], outputs=predictions)
        #first freez everything and train few epochs the dense layers
        model.compile(optimizer='rmsprop', loss='categorical_crossentropy',metrics=['accuracy']) 
         
        model.fit_generator(generator= my_training_generator,epochs=3, workers=multiprocessing.cpu_count(),
                            use_multiprocessing=True,shuffle=True,class_weight=None)
                     
        #for i, layer in enumerate(model.layers):
        #    print(i, layer.name)
           
        for layer in model.layers[:30]:
            layer.trainable = False
        for layer in model.layers[30:]:
            layer.trainable = True
            
        nadam=Nadam(lr= self.learning_rate, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004) # decay=0.004
        model.compile(optimizer=nadam, loss='categorical_crossentropy',metrics=['accuracy'])
        
        callbacks_list = self.defineCallbacks(['tensorboard'])
                
        #model.load_weights("model_weights/fineTune_context_infty_48/weights-improvement-88-0.1369.hdf5", by_name=False)
        model.fit_generator(generator=my_training_generator,epochs=self.n_epochs,initial_epoch=0,validation_data=my_validation_generator,
                                    workers=multiprocessing.cpu_count(), use_multiprocessing=True,
                                    callbacks=callbacks_list,shuffle=True,class_weight=None) 
        return model        

        
    def fineTune_VGG16_model(self,trainImgPath,testImgPath):
        
        #***********Loading Data**************************        
        my_training_generator=data_gen(trainImgPath,self.train_labels,batch_size=self.b_size,dataset_name=self.dataset) # 64 or 32
        my_validation_generator=data_gen(testImgPath,self.valid_labels,batch_size=64,dataset_name=self.dataset) # 64 or 32    
        
        #***************************************************
        base_model = VGG16(weights='imagenet', include_top=False,input_shape = self.dim)        
        # add a global spatial average pooling layer
        x = base_model.output
        x = GlobalAveragePooling2D()(x)
        x = Dense(1024, activation='relu')(x)
        x= Dropout(0.5)(x) 
        # and a logistic layer -- let's say we have 206 classes or 213??
        predictions = Dense(self.n_classes, activation='softmax')(x)
        
        # this is the model we will train
        model = Model(inputs=base_model.input, outputs=predictions)
        
        for layer in base_model.layers:
            layer.trainable = False
            
        #rmsprop=RMSprop(lr=0.00001, rho=0.9, epsilon=None, decay=0.0)    
        model.compile(optimizer='rmsprop', loss='categorical_crossentropy',metrics=['accuracy']) 
         
        model.fit_generator(generator= my_training_generator,epochs=1, workers=multiprocessing.cpu_count(),
                            use_multiprocessing=True,shuffle=True,class_weight=None)     
                                           
        #model.save_weights('fineTuneInftyC.h5')
        #model.load_weights('fineTune_crohmep1.h5')        
        #*********************************************************************************************
    
        for layer in model.layers[:15]:
            #print layer.name
            layer.trainable = False
        for layer in model.layers[15:]:
            layer.trainable = True
                        
        nadam=Nadam(lr= self.learning_rate, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004) # decay=0.004
        #sgd = SGD(lr=1e-3, decay=1e-6, momentum=0.9, nesterov=True) optimizer sgd?
        #rmsprop=RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)
        model.compile(optimizer=nadam, loss='categorical_crossentropy',metrics=['accuracy'])
 
        callbacks_list = self.defineCallbacks(['tensorboard','checkpoint'])
                
        #model.load_weights("model_weights/fineTune_context_infty_48/weights-improvement-88-0.1369.hdf5", by_name=False)
        model.fit_generator(generator= my_training_generator,epochs=self.n_epochs,initial_epoch=0,validation_data=my_validation_generator,
                                    workers=multiprocessing.cpu_count(), use_multiprocessing=True,
                                    callbacks=callbacks_list,shuffle=True,class_weight=None) 
        return model        

    def evaluate_model(self,model,testImgPath1,testImgPath2=None):
        
        if testImgPath2:
            #model.load_weights("model_weights/fusion_t224_c224/weights-improvement-27-0.0265.hdf5", by_name=False)
            my_testing_generator=data_gen_2D(testImgPath1,testImgPath2,self.test_labels,batch_size=64,dataset_name=self.dataset)         
        else:
            my_testing_generator=data_gen(testImgPath1,self.test_labels,batch_size=64,dataset_name=self.dataset)

        scores=model.evaluate_generator(my_testing_generator, steps=None, max_queue_size=15, workers=multiprocessing.cpu_count(),
                           use_multiprocessing=False) 
        ''' 
        prediction=model.predict_generator(generator= my_testing_generator, steps=None,
                                           max_queue_size=10, workers=1, use_multiprocessing=False, verbose=0)
        
        #self.error_analysis(prediction)
        
        #MM: to get the recognition after preprocessing
        self.merge_similar_classes(prediction)
        '''
        return scores
        
    def error_analysis(self,prediction):

        func1,func2=self.funcs
        y_pred_numerical=np.argmax(prediction,axis=1)
        y_pred_cat=func1.inverse_transform(y_pred_numerical)
        y_pred=[str(i) for i in y_pred_cat]
        
        y_test=[i[0] for i in self.test_labels.values()]
        
        self.confusion_matrix_error(y_test,y_pred)
            
    @staticmethod        
    def collapse_classes(label_list):
        for i,label in enumerate(label_list):
            if label in ['fractionalLine','minus','overline','hyphen']:
                label_list[i]='minus'
            if label in ['ldots', 'cdots']:
                label_list[i]= 'ldots'
            if label in ['dot','cdot','period']:
                label_list[i]= 'dot'
        return label_list
    
    @staticmethod
    def confusion_matrix_error(y_test,y_pred):
        classes=list(np.unique(np.array(y_test)))    
        C=confusion_matrix(y_test,y_pred, labels=classes)
        np.fill_diagonal(C, 0)
        err=np.sum(C,axis=1)
        err_index=np.argsort(err)[::-1]
        for i in err_index[:10]:
            #finding the biggest error for this class
            wrong_cl_index=np.argmax(C[i,:])
            wrong_cl=classes[wrong_cl_index]
            print (classes[i],'mistaken with:',wrong_cl)
            print('total error',err[i])
            print ('====================')
    #merge dots and lines as a post processing step        
    def merge_similar_classes(self,prediction):
        from sklearn.metrics import accuracy_score
        func1,func2=self.funcs
        y_pred_numerical=np.argmax(prediction,axis=1)
        y_pred_cat=func1.inverse_transform(y_pred_numerical)
        y_pred=[str(i) for i in y_pred_cat]
           
        y_test=[i[0] for i in self.test_labels.values()]

        new_pred= self.collapse_classes(y_pred)        
        new_test=self.collapse_classes(y_test)
        rate=accuracy_score(new_test,new_pred)
        print ('Recognition Rate after merging',rate)
        
        #error analysis after post processing
        self.confusion_matrix_error(new_test,new_pred)
     
    def defineCallbacks(self,callback_types):
        callback=[]
        if 'checkpoint' in callback_types:
            weightPath='../output/model_weights/classification/'+self.experimentName
            if not os.path.isdir(weightPath):
                os.mkdir(weightPath)
            filepath=weightPath+"/weights-improvement-{epoch:02d}-{loss:.4f}.hdf5"
            checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
            callback.append(checkpoint)

        if 'earlyStopping' in callback_types:    
            earlystopping= EarlyStopping(monitor='val_loss',patience=3,restore_best_weights=True)
            callback.append(earlystopping)

        if 'tensorboard' in callback_types:
            logPath="../output/logs/rec_logs/INFTY/"+self.experimentName
            if not os.path.isdir(logPath):
                os.mkdir(logPath)
            tensorboard = TensorBoard(log_dir=logPath)
            callback.append(tensorboard)

        return callback       
