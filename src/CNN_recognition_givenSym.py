#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 12:22:18 2018

@author: mxm7832
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec  2 15:43:49 2018

@author: mxm7832
"""


import matplotlib.pyplot as plt
import numpy as np
import sys
import os
import shutil
from recognizer.control.controller import Controller
from recognizer.configuration.configuration import Configuration
from recognizer.data.dataset_builder import TYPES_DATA_SET
# from extract_CCpair_info import write_pairInfo, write_pairFeat
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES

#from recognizer.recognition import symbol_parser


def main():    
    #config = Configuration.from_file(sys.argv[1])
    configPath='configs/CNNs/full_system_infty_symbols.conf'
    config = Configuration.from_file(configPath)
       
    subconfig_dir = config.get_str("SUB_CONFIGS_DIR")
    subconfigs_names = config.get("SUB_CONFIGS")

    control = Controller(config, "default")

    experts_names = config.get("CNN_EXPERT_LIST")
    expert_dir = config.get_str("CNN_EXPERT_DIR")

    ### Load Test Expressions ###
    print("Loading Test Expressions")
    control.loadExpressions(name="testing", isTraining=False)    
    control.preprocessExpressions("testing")
    control.createExpressionGraphs("testing")
#############################################################################
    ### Do Parsing ###
############################################################################# 
    ### Add subConfigs ###
    par_config = Configuration.from_file(subconfig_dir+subconfigs_names[2]+".conf")
    control.addConfig(par_config, "par_config")
    
    # Get the trained model (parser)
    print("Loading Parser")
    control.load_cnnExpert(expert_dir+experts_names[2]+".h5",task=TYPES_DATA_SET.Relation.value, name=experts_names[2])

   # Get Test Relation features
    print("Creating Test Parsing geometric Features")
    control.createDataset("testing", TYPES_DATA_SET.Relation, label_source=experts_names[2],
                                         dataset_name="par_data_testing", config_name="par_config")

    #Making pair images to fed into generator
    control.generateImage_CNN("par_config",TYPES_DATA_SET.Relation.value,
                              featureSet_name="par_data_testing")#configname,task, featureSet_name

    print("Parsing Expressions")    
    control.parse_CNN(experts_names[2], "par_config", "par_data_testing",labels=TYPES_DATA_SET.Relation.value)

   # Clean After Parsing
    del control.datasets["par_data_testing"]
    del control.CNNexperts[experts_names[2]]
    shutil.rmtree((control.configs["par_config"].get_str('CNN_IMAGE_OUTPUT_DIR')))
    
##################################################################################
    ### classifying symbol nodes
##################################################################################
    # add sub configs
    symRec_config = Configuration.from_file(subconfig_dir+subconfigs_names[1]+".conf")
    control.addConfig(symRec_config, "sym_config")
    
    #Merge 213 classes to 207
    control.merge_symbol_labels("testing")
    
    # Get the trained model 
    print("Loading Symbol Classifier")
    control.load_cnnExpert(expert_dir+experts_names[1]+".h5",task=TYPES_DATA_SET.Symbol.value, name=experts_names[1])
    
    #Making symbol images to fed into generator
    control.generateImage_CNN("sym_config",TYPES_DATA_SET.Symbol.value)#configname,task

    print("Classifying Symbols")
    control.symbolRec_CNN(experts_names[1],"sym_config")

   # Clean After symbol classification
    del control.CNNexperts[experts_names[1]]
    shutil.rmtree((control.configs["sym_config"].get_str('CNN_IMAGE_OUTPUT_DIR')))
    
##############################################################################################
    #give nodes the GT labels 
##############################################################################################    
    '''
    print("Outputting Label Graph Files")
    expressions = control.getExpressions("testing")
    for exprId in expressions.keys():
        expr = expressions[exprId]
        for symbol in expr.expressionGraph.nodes():
            expr.expressionGraph.node[symbol][NODE_ATTRIBUTES.PREDICTED_CLASS] = expr.expressionGraph.node[symbol][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS]
        expressions[exprId] = expr
    control.expressionSets['testing'] = expressions
    '''
   ### Output  Final results ###
    control.outputExpressions("testing", location=config.get_str("OUTPUT_DIR"), lg_type="OR")
    
if __name__ == "__main__":
    main()
















