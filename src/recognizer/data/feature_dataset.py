
import pickle
import numpy as np
from sklearn.preprocessing import StandardScaler


class FeatureDataset:

    def __init__(self, data, labels, label_mapping, class_mapping, dataset_type, sources=None, config=None):
        # main data
        self.data = data
        self.labels = labels
        self.label_mapping = label_mapping
        self.class_mapping = class_mapping
        #
        # optional ...
        self.sources = sources
        self.config = config
        self.normalizer = None
        self.dataset_type = dataset_type

    def normalize_data(self, normalizer=None):
        if normalizer is None:
            # train new normalizer
            self.normalizer = StandardScaler()
            self.data = self.normalizer.fit_transform(self.data)
        else:
            # use the provided normalizer
            self.normalizer = normalizer
            self.data = self.normalizer.transform(self.data)

    def save_to_file(self, filename):
        out_file = open(filename, 'wb')
        pickle.dump(self, out_file, pickle.HIGHEST_PROTOCOL)
        out_file.close()

    def num_samples(self):
        return self.data.shape[0]

    def num_attributes(self):
        return self.data.shape[1]

    def num_classes(self):
        return len(self.label_mapping)


    def split_data(self, n_partitions):
        by_class = np.argsort(self.labels, axis=0)
        partitions_data, partitions_labels, partitions_indices, partition_sources = [], [], [],[]
        for i in range(n_partitions):
            partition_indices = np.squeeze(by_class[i::n_partitions])
            partitions_data.append(self.data[partition_indices])
            partitions_labels.append(self.labels[partition_indices])
            partitions_indices.append(partition_indices)
            if(self.sources != None):
                partition_sources_i = [self.sources[j] for j in partition_indices]
            else:
                partition_sources_i = None
            partition_sources.append(partition_sources_i)
        return partitions_data, partitions_labels, partitions_indices, partition_sources

    @staticmethod
    def from_INKML_data(raw_features, raw_labels, label_mapping=None, class_mapping=None, dataset_type="", sources=None, config=None):
        # create data
        data, sources = np.array(raw_features),np.array(sources)

        # create label mapping, and map raw labels
        mapped_labels, label_mapping, class_mapping = FeatureDataset.create_update_label_mapping(raw_labels, label_mapping, class_mapping)
        return FeatureDataset(data, mapped_labels, label_mapping, class_mapping, dataset_type, sources, config)

    @staticmethod
    def from_Img_data(raw_features, raw_labels, label_mapping=None, class_mapping=None, sources=None, config=None):
        data, sources = np.array(raw_features), np.array(sources)

        mapped_labels, label_mapping, class_mapping = FeatureDataset.create_update_label_mapping(raw_labels, label_mapping, class_mapping)
        return FeatureDataset(data, mapped_labels, label_mapping, class_mapping, sources, config)

    @staticmethod
    def create_update_label_mapping(raw_labels, label_mapping=None, class_mapping=None):
        if label_mapping is None:
            modify_mapping = True
            label_mapping = []
            class_mapping = {}
        else:
            modify_mapping = False

        mapped_labels = np.zeros((len(raw_labels), 1), dtype = np.int32)
        for idx, class_label in enumerate(raw_labels):
            if not class_label in class_mapping:
                class_mapping[class_label] = len(label_mapping)
                label_mapping.append(class_label)
                if not modify_mapping:
                    print("Warning: new class <" + class_label + "> added to existing mapping")


            mapped_labels[idx, 0] = class_mapping[class_label]

        return mapped_labels, label_mapping, class_mapping

    @staticmethod
    def maskLabels(dataset, mask):
        labels = np.ravel(dataset.labels)
        label_mapping = dataset.label_mapping # List of all symbol class labels
        class_mapping = dataset.class_mapping # Map Class Label -> index in label map
        masked_labels = np.zeros((len(labels), 1), dtype = np.int32)
        for idx, l in enumerate(labels):
            class_label = label_mapping[l]
            if class_label in mask:
                masked_label = mask[class_label]
            else:
                masked_label = mask["default_label"]
            if not masked_label in class_mapping:
                class_mapping[masked_label] = len(label_mapping)
                label_mapping.append(masked_label)

            masked_labels[idx, 0] = class_mapping[masked_label]

        return masked_labels, label_mapping, class_mapping

    @staticmethod
    def checkMaskEquals(dataset, mask):
        '''
        if the mask on the dataset would work or not
        => if the dataset already contains the "tobe" masked labels
        :param dataset:
        :param mask:
        :return:
        '''
        dataset_labels = dataset.class_mapping.keys()
        masked_labels = list(mask.values())
        for label in dataset_labels:
            if not label in masked_labels:
                return False
        return True

    @staticmethod
    def createNewMaskMapping(dataset, mask):
        '''
        Creates a new mask
        :param dataset:
        :param mask:
        :return:
        '''
        if FeatureDataset.checkMaskEquals(dataset, mask):
            return dataset.labels, dataset.label_mapping, dataset.class_mapping
        labels = np.ravel(dataset.labels)
        label_mapping = dataset.label_mapping  # List of all symbol class labels
        new_label_mapping = []
        new_class_mapping = {}
        masked_labels = np.zeros((len(labels), 1), dtype=np.int32)
        for idx, l in enumerate(labels):
            class_label = label_mapping[l]
            if class_label in mask:
                masked_label = mask[class_label]
            else:
                masked_label = mask["default_label"]

            if not masked_label in new_class_mapping:
                new_class_mapping[masked_label] = len(new_label_mapping)
                new_label_mapping.append(masked_label)

            masked_labels[idx, 0] = new_class_mapping[masked_label]

        return masked_labels, new_label_mapping, new_class_mapping

    @staticmethod
    def load_from_file(filename):
        in_file = open(filename, 'rb')
        dataset = pickle.load(in_file)
        in_file.close()
        return dataset
