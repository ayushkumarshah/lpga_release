
class Clustering:
    DistanceEuclidean = 0
    DistanceCosine = 1
    DistanceCustom = 2
    CentersArithmetic = 0
    CentersDataPoints = 1

    @staticmethod
    def k_means_clustering(data, k, distance_type, center_type, custom_distance=None, min_improvement=0.001, max_iterations=10000):
        last_improvement = min_improvement + 0.1
        current_iteration = 1

        while last_improvement > min_improvement and current_iteration < max_iterations:
            pass


    @staticmethod
    def make_unit_vectors(data):
        raise Exception("Not yet implemented")
