import sys
from skimage import io
import numpy as np
from concurrent.futures import ProcessPoolExecutor
from recognizer.data.feature_dataset import FeatureDataset
from recognizer.symbols.math_symbol_trace import MathSymbolTrace
from recognizer.symbols.math_trace_set import MathSymbolTraceSet
from recognizer.symbols.math_expression import MathExpression
from recognizer.preprocessing.trace_smoother import TraceSmoother
from recognizer.preprocessing.image2traces import Image2Traces
from recognizer.preprocessing.cc_ops import CCOps
import matplotlib.pyplot as plt
import os
from PIL import Image
#import autodebug



class ImgLoader:
        #MM: initiate the input image type to use a different loading function for images from pdfs
    def __init__(self, config):
        self.input_type = config.get_str("INPUT_IMG_TYPE").lower() #mm: pdf or img
        self.save_symbols=config.get_bool("GENERATE_SYMBOL_DATASET", False)

    #MM: loading info from pdf LG files
    @staticmethod
    def loadExpressionLabels_pdf(fileId, config):
        symLabels, relLabels, comps, boxes = {}, {}, {}, {}
        lgPath = config.get_str("IMAGE_PROCESSING_LG_PATH")
        lg = open(lgPath + fileId + ".lg")

        for line in lg:
            parts = [p.strip(",") for p in line.strip().split(" ")]
            if parts[0].lower() == "o":
                symLabels[parts[1]] = parts[2]
                comps[parts[1]] = parts[4:]
            elif parts[0].lower() == "r":
                relLabels[(parts[1], parts[2])] = parts[3]
            elif parts[0].lower() == "#sym": #MM: getting symbol bboxes instead of CC bboxes
                boxes[parts[1]] = np.array(parts[2:]).astype(int)
                #print('box id', parts[1])
                #print('symbol boxes',np.array(parts[2:]).astype(int))
        return symLabels, relLabels, comps, boxes



    @staticmethod
    def loadExpressionLabels(fileId, config):
        symLabels, relLabels, comps, boxes = {}, {}, {}, {}
        lgPath = config.get_str("IMAGE_PROCESSING_LG_PATH")
        lg = open(lgPath + fileId + ".lg")

        for line in lg:
            parts = [p.strip(",") for p in line.strip().split(" ")]
            if parts[0].lower() == "o":
                symLabels[parts[1]] = parts[2]
                comps[parts[1]] = parts[4:]
            elif parts[0].lower() == "r":
                relLabels[(parts[1], parts[2])] = parts[3]
            elif parts[0].lower() == "#cc":
                boxes[parts[1]] = np.array(parts[2:]).astype(int)
                #print('box id', parts[1])
                #print('cc boxes', np.array(parts[2:]).astype(int))
        return symLabels, relLabels, comps, boxes
    '''
    #MM: generating symbol images seperated
    def symbol_data_generator(self,data):
        
        fileid, config = data
        #MM: handling diffenrt lg files for images and pdfs
        if self.input_type =='img':
            symLabels, symRels, segs, boxes = self.loadExpressionLabels(fileid, config)
            
        elif self.input_type =='pdf':
            symLabels, symRels, segs, boxes = self.loadExpressionLabels_pdf(fileid, config)

        imgs = self.loadExpressionImages(fileid, boxes, config,save_symbols)
               
        #generate dataset from 
        save_symbols=config.get_bool("GENERATE_SYMBOL_DATASET", True)
        if save_symbols:
            ImgLoader.save_images(imgs,config) 
    '''        

    #@staticmethod
    def loadExpressionImages(self,fileId, boxes, config):
        imgs = {}
        imgsPath = config.get_str("IMAGE_PROCESSING_IMG_PATH")
        expImage = io.imread(imgsPath + fileId + ".PNG", as_gray=True).astype(bool)
       # print('imgInfo', fileId, expImage.shape)
        for id,box in boxes.items():
            #MM: one lg file in training did not have bbox info e.g. 28000954.lg
            if len(box)==0:
                print('------------------ bbox info is missing for this symbol!' )
                box=[1 ,1, 1 ,1]
            if self.input_type.lower()=='img': #MM: produce one cc per image
                imgs[id] = np.array(CCOps.extractComp(expImage, box))
                #TODO: clean symbol images
            if self.input_type.lower()=='pdf': #crop the bbox (can have multiple CCs)
                imgs[id] = np.array(CCOps.extractComp_pdf(expImage, box))
 
        return imgs

    @staticmethod
    def processImg(img, offset, config):
        traceBuilder = Image2Traces(config)
        raw_traces = traceBuilder.createTraces(img)

        traces = {}
        for tid, tp in enumerate(raw_traces):
            trace = MathSymbolTrace(tid, list(map(tuple,[t[::-1] + np.array(offset[::-1]) for t in tp])))
            # TraceSmoother.applyPreprocessing(trace) # Should preprocessing be moved to processing module
            traces[trace.id] = trace 
            # same as traces[tid] = trace since trace.id is same as tid
        return traces

    #@staticmethod
    def create_traceSet(self,fileName,id, compIds, components, offsets, label, config):
        if(len(components) == 0) :
            return MathSymbolTraceSet(id, [MathSymbolTrace("-1", [[0,0]])], label)
        img, offset = CCOps.combineComps(components,np.array(offsets)) #MM: combine components (images of CCs ) to gnerate symbol images
        #MM: saving img of symbols before making traces
        #print(self.save_symbols)
        if self.save_symbols:
            output_dir=config.get_str("SYMBOL_DATASET_PATH")
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)
            im=Image.fromarray(img*255)
            im=im.convert("L")
            im.save(output_dir+fileName+'_'+id+'.PNG')
            print("images of isolated symbols is generated for", fileName)

         #TODO: write the labels in a file or array as gt             
        traces = self.processImg(img, offset, config)

        symbol = MathSymbolTraceSet(id, list(traces.values()), label)
        symbol.setTraceList(compIds)

        return symbol


    #@staticmethod
    def create_expression(self,data):
        #import pdb; pdb.set_trace()
        fileid, config = data
        #print('MM',fileid)
        #MM: handling diffenrt lg files for images and pdfs
        if self.input_type =='img':
            symLabels, symRels, segs, boxes = self.loadExpressionLabels(fileid, config)
            #print(boxes.items())
        elif self.input_type =='pdf':
            symLabels, symRels, segs, boxes = self.loadExpressionLabels_pdf(fileid, config)
            #print(boxes.items())
        #symLabels, symRels, segs, boxes = ImgLoader.loadExpressionLabels(fileid, config)
        imgs = self.loadExpressionImages(fileid, boxes, config) #CC images
        
        '''    
        if fileid=='28000003':
            plt.imshow(imgs['22169'])
            plt.show()        
        '''
        level = config.get_str("GRAPH_LEVEL", "symbol").lower()

        expression = None
        components = {}
        if(level == "primitive"):
            for id,img in imgs.items():
                traces = list(self.processImg(img, boxes[id][:2], config).values())
                components[id] = MathSymbolTraceSet(id, traces, None)
                components[id].trace_list = [id]

                # Create "Stroke" level ground truth symbol class and relation dictionaries
                strokeLabels = {}
                strokeRels = {}
                for symId, label in symLabels.items():
                    for ccId in segs[symId]:
                        strokeLabels[ccId] = label
                        for otherCC in segs[symId]:
                            if ccId != otherCC:
                                strokeRels[(ccId, otherCC)] = label
                for (pid, cid), label in symRels.items():
                    if pid not in segs or cid not in segs:
                        continue
                    for p in segs[pid]:
                        for c in segs[cid]:
                            strokeRels[(p,c)] = label

            expression = MathExpression(fileid, components, strokeLabels, strokeRels, segs, fileid)

        elif(level == "symbol"):
            
            for symId,label in symLabels.items():
                components[symId] = self.create_traceSet(fileid,symId, segs[symId], [imgs[compId] for compId in segs[symId]], [boxes[compId][:2] for compId in segs[symId]], label, config)
                #print(components[symId].__dict__)
                    
            expression = MathExpression(fileid, components, symLabels, symRels, segs,  fileid)        
        '''
        #MM symbol level for pdfs
        elif(level == "symbol" and self.input_type =='pdf'):
            for symId,label in symLabels.items():
                #print('symId', symId)
                #print('compIds',segs[symId])
                #print('Offset (x,y)',boxes[symId][:2])
                components[symId] = self.create_traceSet(symId, symId, [imgs[symId]], [boxes[symId][:2]], label, config)
            #print(components.items())
            expression = MathExpression(fileid, components, symLabels, symRels, segs,  fileid)        
        '''
        return expression
        
    '''
    #MM: loading functions without parallel operation
    #@staticmethod
    def loadExpressions(self,listFile, config, workers=5):
        expressions = {}
        filesIds = []
        for fileid in open(listFile):
            print('FILE ID',fileid)
            filesIds.append((fileid.strip(),config))
        print("Number of Expressions:" + str(len(filesIds)))
        
        for i in range(len(filesIds)):
            exp= self.create_expression(filesIds[i])
            expressions[exp.id] = exp
            print(" Loading: " + str(((i+1)*100)//len(filesIds)) + "%", end="\r")
        print()

        return expressions
    '''
    #@staticmethod
    def loadExpressions(self,listFile, config, workers=5):
        expressions = {}
        filesIds = []
        for fileid in open(listFile):
            #print('FILE ID',fileid)
            filesIds.append((fileid.strip(),config))
        print("Number of Expressions:" + str(len(filesIds)))
        '''
        #MM:remove the parrallel mapper
        loading_function = self.create_expression
        for fileId in filesIds:
            exp=loading_function(fileId)
            expressions[exp.id] = exp
        '''
        
        with ProcessPoolExecutor(max_workers=workers) as executor:
            mapper = executor.map
            loading_function = self.create_expression

            for i, exp in enumerate(mapper(loading_function, filesIds)):
                expressions[exp.id] = exp
                print(" Loading: " + str(((i+1)*100)//len(filesIds)) + "%", end="\r")
            print()
        
        return expressions
    
      #MM: generate symbol dataset for CNN it need a config file in pdf mode
      #better to update it to get CCs and overlay them (later)
    def symbolImg_generation(self,listFile, config, workers=5):
        output_dir=config.get_str("SYMBOL_DATASET_PATH")
        if not os.path.exists(output_dir):
            os.makedirs(output_dir) 
       # print(self.input_type.lower())  
        for fileid in open(listFile):
            print('FILE ID',fileid) 
            fileid=fileid.strip() 
            symLabels, _, _, boxes = self.loadExpressionLabels_pdf(fileid, config)
            # list of images od symbols in one file
            imgList=self.loadExpressionImages(fileid,boxes,config)
            #TODO:MM:write labels in a txt file
            for symid,box in boxes.items():
                im=(imgList[symid]*255).astype(np.uint8)
                im=Image.fromarray(im)
                im=im.convert("L")
                im.save(output_dir+fileid+'_'+symid+'.PNG')
                print("images of isolated symbols is generated for", symLabels[symid])
            #expressions[exp.id] = exp


        return output_dir
    

    # @staticmethod
    # def loadExpressionLabels(fileId, config):
    #     """
    #
    #     :param fileId: Matches the name of the Lg and Png files containing a
    #             list of symbols or an expression
    #     :param imgConfig: Configuration containing parameters and paths
    #             for processing symbol images.
    #     :return: Three lists with each symbol's id, img, and class label
    #     """
    #     ids,imgs, comps, offsets, labels = [], [], [], [], []
    #     imgsPath = config.get_str("IMAGE_PROCESSING_IMG_PATH")
    #     lgPath = config.get_str("IMAGE_PROCESSING_LG_PATH")
    #
    #     imageGrid = io.imread(imgsPath + fileId + ".PNG").astype(bool)
    #     lg = open(lgPath + fileId + ".lg")
    #
    #     notes = []
    #     symLabels = dict([])
    #     symComps = dict([])
    #     for line in lg:
    #         parts = [p.strip(",") for p in line.strip().split(" ")]
    #         if len(parts) >= 6 and parts[0].lower() == "#sym":
    #             ids.append(parts[1])
    #             notes.append(parts[1:])
    #         elif len(parts) >= 4 and parts[0] == "O":
    #             symLabels[parts[1]] = parts[2]
    #             symComps[parts[1]] = parts[4:]
    #
    #     for n in notes:
    #         labels.append(symLabels[n[0]])
    #         ystart, xstart, yend, xend = np.array(n[1:5]).astype(int)
    #         imgs.append(imageGrid[ystart:yend+1,xstart:xend+1])
    #         offsets.append([ystart,xstart])
    #         comps.append(symComps[n[0]])
    #
    #     return ids, imgs, comps, offsets, labels
    
    
    # @staticmethod
    # def process_Lg(data):
    #     """
    #
    #     :param data: tuple containing a file Id, feature extractor object, and recognition configuration.
    #     :return: The file Id, feature vectors for all symbols connected to file id,
    #              list of class labels for all symbols connected to file id.
    #     """
    #     fileId, feature_extractor, config = data
    #     symbols, labels = ImgLoader.create_Symbols(fileId, config)
    #
    #     symbol_data, label_data, source_data = [], [], []
    #     for sym in symbols:
    #         if feature_extractor is not None:
    #             symbol_data.append(feature_extractor.getFeatures(sym))
    #         else:
    #             symbol_data.append(sym)
    #
    #         symbol_label = sym.truth
    #         label_data.append(symbol_label)
    #
    #         # the source of current symbol will stored
    #         source_data.append((fileId, sym.id, sym.trace_list))
    #     return fileId, (symbol_data, label_data, source_data)
    #
    # @staticmethod
    # def load_Lg_List(path, feature_extractor, config, workers=5, save_sources=True, label_mapping=None, class_mapping=None):
    #     """
    #
    #     :param path: To text file containing list of file Ids
    #     :param feature_extractor: feature extractor object created with configuration
    #     :param config: symbol recognition configuration
    #     :param workers: number of workers to use in thread pool
    #     :param label_mapping: list of all class labels with index as their Id
    #     :param class_mapping: dictionary mapping class to their index id
    #     :return: feature data set object with feature vectors from symbols found using fileIds from list,
    #              list of valid files, list of files with errors.
    #     """
    #     data, labels, valid, error = [], [], [], []
    #     sources = [] if save_sources else None
    #     dir_data = []
    #     for fileId in open(path):
    #         dir_data.append((fileId.strip(), feature_extractor, config))
    #
    #     with ProcessPoolExecutor(max_workers=workers) as executor:
    #
    #         mapper = executor.map
    #         loading_function = ImgLoader.process_Lg
    #
    #         for i, (fileId, file_data) in enumerate(mapper(loading_function, dir_data)):
    #             symbol_data, label_data, source_data = file_data
    #             data += symbol_data
    #             labels += label_data
    #             valid.append(fileId.strip())
    #
    #             if save_sources:
    #                 sources += source_data
    #
    #     if feature_extractor is not None:
    #         dataset = FeatureDataset.from_Img_data(data, labels, label_mapping=label_mapping, class_mapping=class_mapping, sources=sources, config=config)
    #     else:
    #         dataset = (data, labels, sources)
    #
    #     return dataset, valid, error



    # @staticmethod
    # def process_Img(id, img, comps, offset, label, config):
    #     """
    #     Takes the symbol image and data and creates the processed traces for
    #      the math symbol object from the symbol image. Traces are preprocessed
    #      and added to a math symbol.
    #
    #     :param id: Unique Symbol Id
    #     :param img: binary image of symbol
    #     :param comps: list of connected component ids for symbol
    #     :param offset: symbols offset in the expression image [lowest y, lowest x]
    #     :param label: The ground truth symbol class label
    #     :param config: Configuration containing parameters and paths
    #             for processing symbol images.
    #     :return: Math Symbol Object
    #     """
    #     traceBuilder = Image2Traces(config)
    #     raw_traces = traceBuilder.createTraces(img)
    #
    #     traces = []
    #     for tid, tp in enumerate(raw_traces):
    #         trace = MathSymbolTrace(tid, list(map(tuple,[t[::-1] + np.array(offset[::-1]) for t in tp])))
    #         TraceSmoother.applyPreprocessing(trace)
    #         traces.append(trace)
    #
    #     symbol = MathSymbol(id, traces, label)
    #     symbol.setTraceList(comps)
    #
    #     return symbol


    # @staticmethod
    # def create_Symbols(fileId, config):
    #     """
    #
    #     :param fileId: Matches the name of the Lg and Png files containing a
    #             list of symbols or math expression
    #     :param imgConfig: Configuration containing parameters and paths
    #             for processing symbol images.
    #     :return: Two lists: list of math symbol objects, list of class labels
    #     """
    #     ids, imgs, comps, offsets, labels = ImgLoader.loadExpressionLabels(fileId, config)
    #     symbols = []
    #     for i in range(0, len(imgs)):
    #         symbols.append(ImgLoader.process_Img(ids[i], imgs[i], comps[i], offsets[i], labels[i], config))
    #
    #     if config.get_bool("SAVE_EXPRESSION_CONTEXT", default=True):
    #         expression = MathExpression(fileId, symbols, fileId)
    #         avg_width = expression.getSizes()[:,0].mean()/len(symbols)
    #         avg_height = expression.getSizes()[:,1].mean()/len(symbols)
    #         for symbol in symbols:
    #             symbol.set_expression(expression)
    #             symbol.set_file_name(fileId)
    #             symbol.setSizeRatio(avg_width, avg_height)
    #             symbol.normalize()
    #     else:
    #         for symbol in symbols:
    #             symbol.normalize()
    #             symbol.set_file_name(fileId)
    #
    #     return symbols, labels

