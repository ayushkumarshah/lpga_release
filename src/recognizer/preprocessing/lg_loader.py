import fnmatch
import re
from recognizer.symbols.math_relation import Relation


class LGLoader:

    @staticmethod
    def get_Relations(lg_file_name):
        '''
        from which relation has to be read :param lg_file:
        relation list (as relation objects) :return:
        '''
        relation_template = re.compile(r"(?P<E>E),\s*(?P<Parent>\d+),\s*(?P<Child>\d+),\s*(?P<Relation>.+),\s*(?P<weight>\d+\.\d+)")
        relations = []
        lg_file = open(lg_file_name, "r")

        for line in lg_file:

            output = relation_template.match(line)
            if output is not None:
                parent_id = output.groupdict()['Parent']
                child_id = output.groupdict()['Child']
                relation =  str(output.groupdict()['Relation'])
                relations.append([str(parent_id), str(child_id), relation])
        return relations

    @staticmethod
    def get_trace_truth(lg_file_name):
        node_template = re.compile(r"(?P<N>N),\s*(?P<trace_id>\d+),\s*(?P<ground_truth>.+),\s*(?P<weight>\d+\.\d+)")
        trace_id_truth={}
        lg_file = open(lg_file_name, "r")

        for line in lg_file:
            matched = node_template.match(line)
            if matched is not None:
                trace_id = str(matched.groupdict()['trace_id'])
                ground_truth = matched.groupdict()['ground_truth']
                trace_id_truth[trace_id]=ground_truth
        return trace_id_truth