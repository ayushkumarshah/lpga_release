import numpy as np
import copy
from recognizer.symbols.math_symbol_trace import MathSymbolTrace
import matplotlib.pyplot as plt

plot_path = '/home/lr3179/Workspace/DPRL_Math_Symbol_Recognizer/src/output/Debug_plots/'

def equation_normalizing(equation):
    '''
    Normalize every symbol in height of 200 preserving the aspect ratio
    '''

    
    #Find the entire bound of the equation
    min_x , min_y, max_x, max_y= equation.boundingBox
    eq_height = ()
    normalize_ratio = 200.0/eq_height
    
    ## get the new points after normalization
    stroke_sets = equation.componentMap.values()
    for symbol in stroke_sets:
        #initialize normalized trace 
        normalized_traces=[]
        #calculate for every trace in the symbol
        for trace in symbol.traces:
            points = trace.points
            #translate the coordinates of the points to the left aligned
            points[:,0] = points[:,0] - min_x
            points[:,1] = points[:,1] - min_y
            #scale the coordinates of the points to a given ratio
            points*=normalize_ratio
        symbol.updateTracesBoundingBox()

    equation.intializeSizesOffsets(equation.componentMap)

