
class SVGGenerator:
    @staticmethod
    def saveSymbolAsSVG(symbol, path):
        f = open(path, 'w')

        f.write('<?xml version="1.0" encoding="UTF-8" standalone="no"?>\r\n')
        f.write('<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.0//EN"')
        f.write(' "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">\r\n')
        f.write('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"')
        f.write('   fill-rule="evenodd" height="10.0in" preserveAspectRatio="none" stroke-linecap="round"')

        squared_box = symbol.getSquaredBoundingBox()
        v_x = squared_box[0] - 0.1
        v_y = squared_box[2] - 0.1
        v_w = squared_box[1] - squared_box[0] + 0.2
        v_h = squared_box[3] - squared_box[2] + 0.2
        v_box = str(v_x) + ' ' + str(v_y) + ' ' + str(v_w) + ' ' + str(v_h)
        f.write('   viewBox="' + v_box + '" width="10.0in">\r\n')
        # f.write('   viewBox="-1.1 -1.1 2.2 2.2" width="10.0in">\r\n')

        f.write('<style type="text/css">\r\n')
        f.write('.pen0 { stroke: rgb(0,0,0); stroke-width: 0.005; stroke-linejoin: round; }\r\n')
        f.write('.pen1 { stroke: rgb(255,0,0); stroke-width: 0.005; stroke-linejoin: round; }\r\n')
        f.write('.pen2 { stroke: rgb(0,255,0); stroke-width: 0.005; stroke-linejoin: round; }\r\n')
        f.write('</style>\r\n')

        f.write('<g>\r\n')

        # The strokes...
        for t in symbol.traces:

            polyline = ''
            for p in t.points:
                x, y = p
                polyline += str(x) + "," + str(y) + ' '
                f.write('<circle cx="' + str(x) + '" cy="' + str(y) + '" r="0.01" fill="blue"/>\n')

            f.write('<polyline class="pen0" fill="none" points="' + polyline + '"/>\n')
            """
            if t.segments is None:
                # write the entire polyline...
                f.write('<polyline class="pen0" fill="none" points="' + polyline + '"/>\n')
            else:
                # write the segments...
                for init, end, ang, stype, l in t.segments:
                    pen = 'pen1' if stype == 1 else 'pen2'

                    line = ''
                    for p in range(init, end + 1):
                        x, y = t.points[p]
                        line += str(x) + "," + str(y) + ' '
                    f.write('<polyline class="' + pen + '" fill="none" points="' + line + '"/>\n')
            """

        f.write('</g>\r\n')
        f.write('</svg>\r\n')

        f.close()

    @staticmethod
    def saveSegmentsAsSVG(segments, box, path):
        f = open(path, 'w')

        f.write('<?xml version="1.0" encoding="UTF-8" standalone="no"?>\r\n')
        f.write('<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.0//EN"')
        f.write(' "http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd">\r\n')
        f.write('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"')
        f.write('   fill-rule="evenodd" height="10.0in" preserveAspectRatio="none" stroke-linecap="round"')

        (box_min_x, box_min_y), (box_max_x, box_max_y) = box
        v_x = box_min_x - 0.1
        v_y = box_min_y - 0.1
        v_w = (box_max_x - box_min_x) + 0.2
        v_h = (box_max_y - box_min_y) + 0.2

        v_box = str(v_x) + ' ' + str(v_y) + ' ' + str(v_w) + ' ' + str(v_h)
        f.write('   viewBox="' + v_box + '" width="10.0in">\r\n')

        f.write('<style type="text/css">\r\n')
        f.write('.pen0 { stroke: rgb(0,0,0); stroke-width: 0.005; stroke-linejoin: round; }\r\n')
        f.write('.pen1 { stroke: rgb(255,0,0); stroke-width: 0.005; stroke-linejoin: round; }\r\n')
        f.write('.pen2 { stroke: rgb(0,255,0); stroke-width: 0.005; stroke-linejoin: round; }\r\n')
        f.write('</style>\r\n')

        f.write('<g>\r\n')

        # The segments ...
        for (p1_x, p1_y), (p2_x, p2_y) in segments:
            f.write('<circle cx="' + str(p1_x) + '" cy="' + str(p1_y) + '" r="0.01" fill="blue"/>\n')
            f.write('<circle cx="' + str(p2_x) + '" cy="' + str(p2_y) + '" r="0.01" fill="blue"/>\n')

            polyline = ''
            polyline += str(p1_x) + "," + str(p1_y) + ' '
            polyline += str(p2_x) + "," + str(p2_y) + ' '
            f.write('<polyline class="pen0" fill="none" points="' + polyline + '"/>\n')

        f.write('</g>\r\n')
        f.write('</svg>\r\n')

        f.close()
