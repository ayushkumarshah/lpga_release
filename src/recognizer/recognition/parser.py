import numpy as np
import networkx as nx
import networkx.algorithms as alg

from recognizer.training.trainer import ClassifierTrainer
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES
from recognizer.testing.tester import TestClassification
from enum import Enum
from recognizer.graph_representation.Graph_construction import GraphConstruction

class TYPES_PARSING(Enum):
    Detect_Any_Relations = 1
    Detect_Layout_Relations =2
    Identify_Any_Relations = 2
    Identify_Layout_Relation = 3



class Parser:

    '''
    Parser utility class to have methods -required to obtain the
    '''

    def __init__(self, config, training_dataset, type=TYPES_PARSING.Identify_Any_Relations):
        self.config = config

        '''
        depending on the type of the parser required,
        '''
        if type == TYPES_PARSING.Detect_Any_Relations:
            self.classifier = self.train_any_relationship_detector(training_dataset, config)
            self.trained_classifier = self.classifier.trained_classifier
        elif type == TYPES_PARSING.Detect_Layout_Relations:
            self.classifier = self.train_layout_relationship_detector(training_dataset, config)
            self.trained_classifier = self.classifier.trained_classifier
        elif type == TYPES_PARSING.Identify_Any_Relations:
            self.classifier = self.train_any_relationship_classifier(training_dataset, config)
            self.trained_classifier = self.classifier.trained_classifier
        elif type == TYPES_PARSING.Identify_Layout_Relation:
            self.classifier = self.train_layout_relationship_classifier(training_dataset, config)
            self.trained_classifier = self.classifier.trained_classifier

        self.classes_list = training_dataset.label_mapping
        self.classes_dict = training_dataset.class_mapping

    def train_any_relationship_classifier(self, feature_dataset, config):
        return ClassifierTrainer.train_classifier(feature_dataset, config, get_raw_classifier=False)

    def train_layout_relationship_classifier(self, feature_dataset, config):
        #considers the same symbol relation ('MERGE) as NoRelation
        if 'MERGE' in feature_dataset.class_mapping:
            merge_class=feature_dataset.class_mapping['MERGE']
            No_relation_class = feature_dataset.class_mapping['NoRelation']
            same_symbol_indices = (feature_dataset.labels == merge_class)
            feature_dataset.labels[same_symbol_indices]=No_relation_class

        classifier= ClassifierTrainer.train_classifier(feature_dataset, config, get_raw_classifier=False)
        return classifier

    def train_any_relationship_detector(self, feature_dataset, config):
        # considers the NoRelation as False and rest as True
        no_relation_indices = (feature_dataset.labels == 'NoRelation')
        original_labels = np.array(feature_dataset.labels)
        feature_dataset.labels[no_relation_indices] = False
        feature_dataset.labels[~no_relation_indices] = True
        classifier = ClassifierTrainer.train_classifier(feature_dataset, config, get_raw_classifier=False)
        feature_dataset.labels = original_labels
        return classifier

    def train_layout_relationship_detector(self, feature_dataset, config):
        # considers NoRelation and '*' as False and rest as True
        noRelationLabel=feature_dataset.class_mapping['NoRelation']
        no_relation_indices = (feature_dataset.labels == noRelationLabel)

        #check if 'same symbol' classes are identified
        if 'MERGE' in feature_dataset.class_mapping:
            sameClassLabel = feature_dataset.class_mapping['MERGE']
            no_relation_indices = np.logical_or(feature_dataset.labels == noRelationLabel, feature_dataset.labels==sameClassLabel)



        original_labels = np.array(feature_dataset.labels)
        feature_dataset.labels[no_relation_indices] = False
        feature_dataset.labels[~no_relation_indices] = True
        classifier = ClassifierTrainer.train_classifier(feature_dataset, config, get_raw_classifier=True)
        feature_dataset.labels = original_labels
        return classifier


    '''
    Prediction - Classes
    '''
    def predict_relation_classes(self, test_dataset):
        #the class and it's confidence
        predicted_output = TestClassification.predict_classes(self.classifier, test_dataset)
        return predicted_output

    def predict_relation_classes_confidence(self, test_dataset):
        '''
        Predict the class-label, confidence for every row in test dataset
        :param classifier: classifier to classify the dataset
        :param dataset: N instances to be classified
        :return: N labels, N confidence values
        '''
        predicted_output, confidence = TestClassification.predict_classes_with_confidence(self.classifier, test_dataset)
        return predicted_output, confidence

    def predict_real_relation_confidence(self, test_dataset):
        '''
        ignore a class_value(No-Relation) and predict the best possible class labels from the remaining classes
        :param classifier:
        :param test_dataset:
        :return:
        '''
        predicted_output, confidence, evaluation_metrics = TestClassification.predict_classes_and_confidence_except(self.classifier, test_dataset, 'NoRelation')
        return predicted_output, confidence

    def detect_relations_above_threshold(self, test_dataset, threshold):
        '''
        returns true or false based on the confidence value (for false values 1- threshold) being greater than threshold
        :param classifier:
        :param test_dataset:
        :param threshold:
        :return:
        '''
        predicted_output, confidence = TestClassification.predict_classes_with_confidence(self.classifier, test_dataset)
        false_indices = (predicted_output == False)
        less_confident_false_values = confidence[false_indices] < (1-threshold)
        #change the less confident "false" predictions to be True
        predicted_output[less_confident_false_values] = True
        return predicted_output

    def predict_true_confidence(self, test_dataset):
        predicted_output, confidence = TestClassification.predict_classes_with_confidence(self.classifier, test_dataset)
        false_indices = (predicted_output == False)
        #revert the confident values for false indices
        confidence[false_indices]  = 1 - confidence[false_indices]
        return confidence


    '''
    Update Layout graph by writing predicting labels and weight
    '''
    def update_layout_relation_in_graph(self, dataset, predicted_labels, confidences, expression_map, extractMst=True):
        '''
        for item in the dataset, updates the corresponding the layout graph
        :param expression_map: expression_file_names mapped to expression objects
        :param dataset: the source, feature, ground_truth label
        '''
        length_dataset = len(dataset.sources)
        entire_data = zip(dataset.sources, predicted_labels, confidences)
        for i, data in enumerate(entire_data):
            #[user update console message] compute the amount of dataset processed
            advance = float(i) / length_dataset
            #data is one edge record in the entire data-set
            #every edge is represented by sourceinformation(Expression_file name, parent-edge-id, child-edge-id
                        #  label predicted for the edge
            source, predicted_label, confidence = data
            expr = expression_map[str(source[0])]
            layout_graph = expr.getLayoutGraph()
            parent_id = str(source[1])
            child_id =str(source[2])
            layout_graph.edge[parent_id][child_id][EDGE_ATTRIBUTES.PREDICTED_LAYOUT] = predicted_label
            layout_graph.edge[parent_id][child_id]['weight'] = confidence
            print(("Graph Representation update => {:.2%}").format(advance), end="\r")


        if extractMst:
            for expr_name,expr in expression_map.items():
                layout_tree=GraphConstruction.get_Maximum_Spanning_Tree(expr.getLayoutGraph())
                GraphConstruction.set_graph_attributes(expr.getLayoutGraph(),layout_tree)
                expr.setLayoutGraph(layout_tree)

        print("\n\nUpdated Labels for all edges in the graph")



    def filter_layout_graph_edges_by_threshold(self, dataset, predicted_chances, expression_map,threshold):
        '''
        for item in the dataset, updates the corresponding the layout graph
        :param expression_map: expression_file_names mapped to expression objects
        :param dataset: the source, feature, ground_truth label
                '''
        length_dataset = len(dataset.sources)
        entire_data = zip(dataset.sources, predicted_chances)
        for i, data in enumerate(entire_data):
            # [user update console message] compute the amount of dataset processed
            advance = float(i) / length_dataset
            # data is one edge record in the entire data-set
            # every edge is represented by sourceinformation(Expression_file name, parent-edge-id, child-edge-id
            #  label predicted for the edge
            source, confidence = data

            layout_graph = expression_map[str(source[0])].getLayoutGraph()
            parent_id = int(source[1])
            child_id = int(source[2])
            if confidence < threshold:
                layout_graph.remove_edge(parent_id,child_id)
            print(("Graph Filtering update => {:.2%}").format(advance), end="\r")

        print("\n\nFiltered edges in the graph")



    def filter_layout_graph_edges(self, dataset, edges_prediction, expression_map):
        '''
        for item in the dataset, removes/adds the ege in the expression graph based on prediction
        :param expression_map: expression_file_names mapped to expression objects
        :param dataset: the source, feature, ground_truth label
                '''
        length_dataset = len(dataset.sources)
        entire_data = zip(dataset.sources, edges_prediction)
        for i, data in enumerate(entire_data):
            # [user update console message] compute the amount of dataset processed
            advance = float(i) / length_dataset
            # data is one edge record in the entire data-set
            # every edge is represented by source information(Expression_file name, parent-edge-id, child-edge-id
            #  label predicted for the edge
            source, prediction = data

            layout_graph = expression_map[str(source[0])].getLayoutGraph()
            parent_id = int(source[1])
            child_id = int(source[2])
            if prediction == False:
                layout_graph.remove_edge(parent_id,child_id)
            print(("Graph Filtering update => {:.2%}").format(advance), end="\r")

        print("\n\nFiltered edges in the graph")



    def resolve_conflicts_in_symbols(self, graph):
        '''
        if two nodes in graph belong to same symbol
        then the common neighbors between them should have the same layout relation
        This conflict in relation is solved by choosing the one with more confidence(weight)
        :param graph:
        :return:
        '''
        edges = graph.edges()
        predicted_relation = nx.get_edge_attributes(graph, EDGE_ATTRIBUTES.PREDICTED_LAYOUT)
        edge_weight = nx.get_edge_attributes(graph, 'weight')

        for edge in edges:
            parent, child = edge
            if predicted_relation[edge] == '*':
                #find common neighbors of parent and child
                common_neighbors = nx.common_neighbors(graph, parent, child)
                for neigh in common_neighbors:
                    if edge_weight[parent, neigh] > edge_weight[child, neigh]:
                        # update the child edge with the relation
                        graph[child][neigh][EDGE_ATTRIBUTES.PREDICTED_LAYOUT]=graph[parent][neigh][EDGE_ATTRIBUTES.PREDICTED_LAYOUT]
                        graph[child][neigh]['weight']=graph[parent][neigh]['weight']

                    else:
                        graph[parent][neigh][EDGE_ATTRIBUTES.PREDICTED_LAYOUT]=graph[child][neigh][EDGE_ATTRIBUTES.PREDICTED_LAYOUT]
                        graph[parent][neigh]['weight']=graph[child][neigh]['weight']



    def recover_edges_of_same_symbol(self, graph):
        edges = graph.edges()
        predicted_relation = nx.get_edge_attributes(graph, EDGE_ATTRIBUTES.PREDICTED_LAYOUT)
        edge_weight = nx.get_edge_attributes(graph, 'weight')
        self.resolve_conflicts_in_symbols(graph)

        for edge in edges:
            parent, child = edge
            if predicted_relation[edge] == '*':
                parent_neighbors = graph.neighbors(parent)
                child_neighbors = graph.neighbors(child)

                for p_n in parent_neighbors:
                    edge_data = graph.get_edge_data(parent, p_n)
                    if not graph.has_edge(child, p_n):
                        graph.add_edge(child,p_n)
                        graph[child][p_n][EDGE_ATTRIBUTES.PREDICTED_LAYOUT]=edge_data[EDGE_ATTRIBUTES.PREDICTED_LAYOUT]
                        graph[child][p_n]['weight']=edge_data['weight']

                for c_n in child_neighbors:
                    edge_data = graph.get_edge_data(child, c_n)
                    if not graph.has_edge(parent, c_n):
                        graph.add_edge(parent,c_n)
                        graph[parent][c_n][EDGE_ATTRIBUTES.PREDICTED_LAYOUT]=edge_data[EDGE_ATTRIBUTES.PREDICTED_LAYOUT]
                        graph[parent][c_n]['weight']=edge_data['weight']



    def update_recovered_edge_set(self, expression_map):
        expression_list = expression_map.values()
        total_expressions = len(expression_list)
        for i, expression in enumerate(expression_list):
            advance = i*100/total_expressions
            print("Recovery done for ==>",advance, end="\r")
            layout_graph = expression.getLayoutGraph()
            self.recover_edges_of_same_symbol(layout_graph)
        print("Conflicts in edges resolved and missing edges recovered!")





